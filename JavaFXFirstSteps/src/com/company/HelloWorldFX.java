package com.company;

//package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * 1 Hello World, JavaFX Style
 * https://docs.oracle.com/javafx/2/get_started/hello_world.htm#CHDIFJHE
 */
public class HelloWorldFX extends Application {
    // The main class for a JavaFX application extends the javafx.application.Application class.
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("JavaFX Welcome");

        // GridPane with Gap and Padding Properties
        GridPane grid = new GridPane();
        // The alignment property changes the default position of the grid from the top left of the scene to the center.
        grid.setAlignment(Pos.CENTER);
        // The gap properties manage the spacing between the rows and columns
        grid.setHgap(10);
        grid.setVgap(10);
        // the padding property manages the space around the edges of the grid pane.
        grid.setPadding(new Insets(25, 25, 25, 25)); //  insets are in the order of top, right, bottom, and left

        // CONTROLS
        // creates a Text object that cannot be edited
        Text scenetitle = new Text("Welcome");
        //scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        // adds the scenetitle variable to the layout grid
        grid.add(scenetitle, 0, 0, 2, 1); // scenetitle is added in column 0, row 0. The last two arguments set the column span to 2 and the row span to 1.

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        // display the grid lines, which is useful for debugging purposes.
        //grid.setGridLinesVisible(true);

        // BUTTONS
        // creates a button named btn with the label Sign in
        Button btn = new Button("Sign in");
        //  creates an HBox layout pane named hbBtn with spacing of 10 pixels
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        // The button is added as a child of the HBox pane,
        hbBtn.getChildren().add(btn);
        // the HBox pane is added to the grid in column 1, row 4.
        grid.add(hbBtn, 1, 4);

        // Add a Text control for displaying the message
        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);

        //Button Event
        // setOnAction() method is used to register an event handler
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                //actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Sign in button pressed");
            }
        });

        // Create an ID for each text node b
        scenetitle.setId("welcome-text");
        actiontarget.setId("actiontarget");

        // The SCENE
        Scene scene = new Scene(grid, 400, 275); //  If you do not set the scene dimensions, the scene defaults to the minimum size needed to display its contents.
        primaryStage.setScene(scene);
        // CSS - Initialize the stylesheets Variable
        scene.getStylesheets().add(getClass().getResource("HelloWorldFX.css").toExternalForm());
        primaryStage.show();
    }
}